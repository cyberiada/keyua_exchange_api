# keyua_exchange_api

Installation
---

    # For production environment:
    # Create virtualenv
    export VENV_DIR=venv
    python3.7 -m venv "${VENV_DIR}"
    "${VENV_DIR}"/bin/python3.7 -m ensurepip --upgrade
    "${VENV_DIR}"/bin/python3.7 -m pip install -r requirements/requirements.txt

    # For development environment:
    make setup

    # Create superuser PostgreSQL for development
    CREATE ROLE keyua WITH SUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'keyua';
    
    # Create database
    CREATE DATABASE keyua_exchange;

    # For testing RabbitMQ I used this docker:
    docker run -d -h node-1.rabbit                                      \
           --name rabbit                                            \
           -p "4369:4369"                                           \
           -p "5672:5672"                                           \
           -p "15672:15672"                                         \
           -p "25672:25672"                                         \
           -p "35197:35197"                                         \
           -e "RABBITMQ_USE_LONGNAME=true"                          \
           -e "RABBITMQ_LOGS=/var/log/rabbitmq/rabbit.log"          \
           -v /data:/var/lib/rabbitmq \
           -v /data/logs:/var/log/rabbitmq \
           rabbitmq:3.6.6-management

Environment variables
---
These environment variables will be defined in any runtime environment and could be used in program code:

    DB_HOST              # DB listen host,      default=localhost 
    DB_PORT              # DB listen port,      default=5432
    DB_NAME              # DB Name,             default=keyua_weather 
    DB_USER              # DB user name,        default=keyua
    DB_PASSWORD          # DB password,         default=keyua


Run tests
---
    # Run flake8:
    make flake

Start app
---
    # Run migrations
    $ alembic upgrade head

    # Run server
    python manage.py runserver  # optional key: -h 0.0.0.0 -p 5000

    # Run consumer
    python manage.py runconsumer

First steps
---
See a Postman collection in a config dir.
