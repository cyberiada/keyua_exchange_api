"""init database

Revision ID: c033e44424e0
Revises: 
Create Date: 2019-06-29 00:41:24.661305

"""
import datetime
import uuid

from alembic import op
from sqlalchemy import Column, DateTime, Integer
from sqlalchemy.dialects.postgresql import UUID


# revision identifiers, used by Alembic.
revision = 'c033e44424e0'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "exchange",
        Column("exchange_uuid", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
        Column("datetime", DateTime, nullable=False, default=datetime.datetime.utcnow()),
        Column("value", Integer, nullable=False),
    )


def downgrade():
    op.drop_table("exchange")

