from flask import Flask

from service_api import api_v1
from service_api.config import runtime_config
from service_api.services.database import Database

app = Flask(__name__)

app_config = runtime_config()
app.config.from_object(app_config)

Database.config_from_object(app_config)

api_v1.load_api(app)
