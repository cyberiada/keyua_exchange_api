from flask_restful import Api

from service_api.constants import SERVICE_NAME


def load_api(app):

    from service_api.resources import SmokeResource
    from service_api.resources.exchange import ExchangesResource

    api_v1 = Api(app, f"/{SERVICE_NAME}/v1")

    api_v1.add_resource(SmokeResource, "/smoke")

    api_v1.add_resource(ExchangesResource, "/exchanges")
