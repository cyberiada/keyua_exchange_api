from http import HTTPStatus

from flask import request

from service_api.resources import BaseResource
from service_api.services.response import json_response
from service_api.services.validation_schema import CreateExchangeSchema
from service_api.domain.exchange import create_exchange, get_exchanges


class ExchangesResource(BaseResource):
    def post(self):
        payload = CreateExchangeSchema().load(request.json)
        created_exchange = create_exchange(payload)
        return json_response(created_exchange), HTTPStatus.CREATED

    def get(self):
        exchanges = get_exchanges()
        return json_response(data=exchanges)
