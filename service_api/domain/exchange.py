from werkzeug.exceptions import NotFound
from sqlalchemy.dialects.postgresql import insert

from service_api.models import Exchange
from service_api.services.database import DatabaseRegistry
from service_api.services.rabbit import MQPublisher
from service_api.services.response import json_dump


def create_exchange(payload):
    db_engine = DatabaseRegistry.get()
    stmt = insert(Exchange).returning(Exchange.c.exchange_uuid, Exchange.c.value).values(**payload)

    with db_engine.connect() as conn:
        cur = conn.execute(stmt)
        result = dict(cur.fetchone())

    with MQPublisher() as publisher:
        publisher.publish(msg=json_dump(result))

    return result


def get_exchanges():
    db_engine = DatabaseRegistry.get()
    stmt = Exchange.select()

    with db_engine.connect() as conn:
        cur = conn.execute(stmt)
        result = cur.fetchall()

    if not result:
        raise NotFound("Exchanges not found")

    return [dict(ex) for ex in result]
