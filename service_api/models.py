import datetime
import uuid

from sqlalchemy import (
    Table,
    MetaData,
    Column,
    DateTime,
    Integer,
)
from sqlalchemy.dialects.postgresql import UUID


metadata = MetaData()


Exchange = Table(
    "exchange",
    metadata,
    Column("exchange_uuid", UUID(as_uuid=True), primary_key=True, default=uuid.uuid4),
    Column("datetime", DateTime, nullable=False, default=datetime.datetime.utcnow()),
    Column("value", Integer, nullable=False),
)


models = (Exchange,)
