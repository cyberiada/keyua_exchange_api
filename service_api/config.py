import os

from service_api.constants import DEFAULT_DB

basedir = os.path.abspath(os.path.dirname(__file__))


class BaseConfig:
    DEBUG = False

    DB_HOST = os.environ.get("DB_HOST", "localhost")
    DB_PORT = os.environ.get("DB_PORT", 5432)
    DB_NAME = os.environ.get("DB_NAME", DEFAULT_DB)
    DB_USER = os.environ.get("DB_USER", "keyua")
    DB_PASSWORD = os.environ.get("DB_PASSWORD", "keyua")
    DB_DSN_FORMAT = "postgresql://{user}:{password}@{host}:{port}/{db}"

    SECRET_KEY = os.environ.get("SECRET_KEY", "keyua")
    JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY", "keyua")


class DevConf(BaseConfig):
    DEBUG = True


class ProdConf(BaseConfig):
    DEBUG = False


ENV_CONFIG = {"dev": DevConf, "prod": ProdConf}


def runtime_config():
    env = os.environ.get("APP_ENV", "dev")
    return ENV_CONFIG[env]
