import datetime
import json
from uuid import UUID


class JsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            return str(obj)
        elif isinstance(obj, datetime.datetime):
            return obj.strftime("%Y-%m-%dT%H:%M:%S")
        return json.JSONEncoder.default(self, obj)


def json_dump(data):
    return json.dumps(data, cls=JsonEncoder)


def json_response(data):
    dumps = json.dumps(data, cls=JsonEncoder)
    return json.loads(dumps)
