import pika

from service_api.constants import EXCHANGE_QUEUE


class RabbitMQ:

    MQ_HOST = "localhost"
    MQ_PORT = 5672
    EXCHANGE = "keyua"
    EXCHANGE_TYPE = "direct"

    def __init__(self, queue_name=EXCHANGE_QUEUE):
        self._connection = None
        self._channel = None
        self.queue_name = queue_name

        self.create_conn()
        self.create_chanel()
        self.exchange_declare()
        self.create_queue()

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self._connection.close()

    def create_conn(self):
        if not self._connection or self._connection.is_closed:
            conn_param = pika.ConnectionParameters(self.MQ_HOST, self.MQ_PORT)
            self._connection = pika.BlockingConnection(conn_param)

    def create_chanel(self):
        if not self._channel:
            self._channel = self._connection.channel()

    def create_queue(self):
        self._channel.queue_declare(queue=self.queue_name)
        self._channel.queue_bind(exchange=self.EXCHANGE, queue=self.queue_name, routing_key=self.queue_name)

    def exchange_declare(self):
        self._channel.exchange_declare(exchange=self.EXCHANGE, exchange_type=self.EXCHANGE_TYPE)


class MQPublisher(RabbitMQ):

    def publish(self, msg):
        self._channel.basic_publish(exchange=self.EXCHANGE, routing_key=self.queue_name, body=msg)


class MQConsumer(RabbitMQ):

    def consuming(self, callback):
        self._channel.basic_consume(on_message_callback=callback, queue=self.queue_name)
        self._channel.start_consuming()
