from sqlalchemy import create_engine


class Database:

    DB_HOST = None
    DB_PORT = None
    DB_USER = None
    DB_PASSWORD = None
    DB_NAME = None
    DB_DSN_FORMAT = None

    def __init__(self, db_name=None):
        self._engine = None
        self.db_name = db_name or self.DB_NAME

    @classmethod
    def config_from_object(cls, configs):
        cls.DB_HOST = getattr(configs, "DB_HOST")
        cls.DB_PORT = getattr(configs, "DB_PORT")
        cls.DB_USER = getattr(configs, "DB_USER")
        cls.DB_PASSWORD = getattr(configs, "DB_PASSWORD")
        cls.DB_NAME = getattr(configs, "DB_NAME")
        cls.DB_DSN_FORMAT = getattr(configs, "DB_DSN_FORMAT")

    @property
    def dsn(self):
        dsn = self.DB_DSN_FORMAT.format(
            host=self.DB_HOST,
            port=self.DB_PORT,
            user=self.DB_USER,
            password=self.DB_PASSWORD,
            db=self.db_name,
        )
        return dsn

    def _create_engine(self):
        self._engine = create_engine(self.dsn)

    def _check_engine(self):
        if not bool(self._engine) or self._engine.closed:
            self._create_engine()

    def get_engine(self):
        self._check_engine()
        return self._engine


class DatabaseRegistry:
    _db_pool = {}

    @classmethod
    def get(cls, db_name=Database.DB_NAME):
        if db_name not in cls._db_pool:
            cls.init(db_name)
        return cls._db_pool[db_name]

    @classmethod
    def init(cls, db_name=None):
        database = Database(db_name)
        cls._db_pool[db_name] = database.get_engine()
