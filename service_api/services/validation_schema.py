from marshmallow import Schema, fields


class BaseSchema(Schema):
    pass


class CreateExchangeSchema(BaseSchema):
    datetime = fields.DateTime()
    value = fields.Integer(required=True)
