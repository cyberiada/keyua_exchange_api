import argparse

from service_api.app import app
from service_api.services.rabbit import MQConsumer


def runserver(host=None, port=None):
    app.run(host, port)


def ui(ch, method, properties, body):
    print(body)
    ch.basic_ack(delivery_tag=method.delivery_tag)


def runconsumer():
    with MQConsumer() as consumer:
        consumer.consuming(callback=ui)


parser = argparse.ArgumentParser(description="Flask RESTFull api skeleton", add_help=False)
subparsers = parser.add_subparsers(dest="command")

sparser = subparsers.add_parser(runserver.__name__, add_help=False, help="Discover all clients dbs")
sparser.add_argument("-h", "--host", dest="host", default="0.0.0.0", type=str, help="Host")
sparser.add_argument("-p", "--port", dest="port", default=5000, type=int, help="Port")

sparser = subparsers.add_parser(runconsumer.__name__, add_help=False, help="Run consumer")


parsed_args = parser.parse_args()


if parsed_args.command == runserver.__name__:
    runserver(parsed_args.host, parsed_args.port)
elif parsed_args.command == runconsumer.__name__:
    runconsumer()
